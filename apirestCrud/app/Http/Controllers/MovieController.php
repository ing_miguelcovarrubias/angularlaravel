<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
  
    public function index()
    {
        $movies = Movie::get();
        echo json_encode($movies);
    }

 
    public function store(Request $request)
    {
        $movie = new Movie($request->all());
        $movie->save();
        echo json_encode($movie);
    }


    public function update(Request $request, $id)
    {
        $movie = Movie::find($id);
        $movie->fill($request->all());
        $movie->save();
        echo json_encode($movie);
    }


    public function destroy($id)
    {
        $movie = Movie::find($id);
        $movie->delete();
    }
}
