import { RouterModule, Routes } from '@angular/router';
import { MoviesComponent } from './components/movies/movies.component';
import { FormComponent } from './components/form/form.component';

const APP_ROUTES: Routes = [
    { path: 'movies', component: MoviesComponent },
    { path: 'form', component: FormComponent },
    { path: 'form/:id', component: FormComponent },
    { path: '**', pathMatch:'full', redirectTo: 'movies' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);