import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { Movie } from "../../interfaces/movie";



@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styles: []
})
export class MoviesComponent implements OnInit {

  //ApiRest de Laravel
  API_ENDPOINT = 'http://localhost:8000/api';
  //Array donde recibire los datos
  movies: Movie[];

  constructor(private movieService: MoviesService) { 
   this.getMovies();
  }

  getMovies(){
    this.movieService.get().subscribe((data: Movie[])=>{
      this.movies = data;
    }, (error)=>{
      alert('Error');
    });
  }

  ngOnInit() {
  }

  delete(id){
    if(confirm('Are you sure?')){
      this.movieService.delete(id).subscribe((data)=>{
        alert('Movie deleted');
        this.getMovies();
      }, (error)=>{
        console.log(error);
      });
    }
    
  }

}
