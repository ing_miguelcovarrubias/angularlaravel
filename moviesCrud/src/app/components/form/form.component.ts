import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie';
import { MoviesService } from 'src/app/services/movies.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styles: []
})
export class FormComponent implements OnInit {

  movie: Movie={
    name:null,
    year:null,
    description:null,
    duration:null,
    genre:null
  }
  //Variables
  id:any
  editing: boolean = false;
  movies: Movie[];

;  constructor(private moviesService: MoviesService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.editing = true;
      this.moviesService.get().subscribe((data: Movie[])=>{
        //Se guardan todos los datos de la base de datos en un array de tipo movie
        this.movies = data;
        //Se guardan los datos en el objeto Movie
        this.movie = this.movies.find((m)=>{return m.id == this.id});
      }, (error)=>{
        console.log(error);
      });
      
    }else{
      this.editing = false;
    }
   }

  ngOnInit() {
  }

  saveMovie(){
    if(this.editing){
      this.moviesService.put(this.movie).subscribe((data)=>{
        alert('Movie updated');
        this.goHome();
      },(error)=>{
        alert('Error');
      });
    }else{
      this.moviesService.save(this.movie).subscribe((data)=>{
        alert('Movie saved');
        this.goHome();
      },(error)=>{
        alert('Error');
      });
    }
    
  }

  goHome(){
    this.router.navigate(['/movies']);
  }

}
