import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Movie } from "../interfaces/movie";

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  //Llamar al ApiRest Laravel
  API_ENDPOINT = "http://localhost:8000/api";

  //Injectar HttpClient
  constructor(private httpClient: HttpClient) { }

  //Obtener Peliculas
  get(){
    //Recibir informacion de la base de datos
    return this.httpClient.get(this.API_ENDPOINT + '/movies');
  };

  //Salvar pelicula nueva
  save(movie: Movie){
    const HEADERS = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/movies', movie, {headers: HEADERS});
  }

  //Actualizar pelicula
  put(movie: Movie){
    const HEADERS = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/movies/' + movie.id, movie, {headers: HEADERS});
  }

  //Borrar pelicula
  delete(id){
    return this.httpClient.delete(this.API_ENDPOINT + '/movies/' + id);
  }

  
}
